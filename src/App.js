import React, { Component } from 'react';
import './App.css';
import { Container } from 'semantic-ui-react';
import HeaderImage from './components/HeaderImage';
import ImageCards from './components/ImageCards';
import Headshot from './components/Headshot';

class App extends Component {
  render() {
    return (
      <div className="App">
          <HeaderImage />
        <Container className="Container">
          <ImageCards/>
        </Container>
        <Container>
          <Headshot />
        </Container>
      </div>
    );
  }
}

export default App;
