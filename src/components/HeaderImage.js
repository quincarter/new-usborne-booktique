import React from 'react';
import { Divider, Image } from 'semantic-ui-react';
import headerImage from '../assets/images/headerImage.png';

const HeaderImage = () =>
    <div>
        <Image src={headerImage} size="large" centered/>
        <Divider hidden />
    </div>;


export default HeaderImage;
