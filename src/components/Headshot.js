/* eslint-disable linebreak-style,no-ternary */
import React from 'react';
import { Image, Header, Icon } from 'semantic-ui-react';
import headshotImage from '../assets/images/headshot.jpg';

let social = {};

const isMobile = {
    'android'() {
        return navigator.userAgent.match(/Android/i);
    },
    'any'() {
        return isMobile.android() || isMobile.blackBerry() || isMobile.iOS() || isMobile.opera() || isMobile.windows();
    },
    'blackBerry'() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    'iOS'() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    'opera'() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    'windows'() {
        return navigator.userAgent.match(/IEMobile/i);
    }
};

if (isMobile.any()) {
    social = {
        // 'facebook': 'https://www.facebook.com/Meredizzle87',
        'email': 'mailto:meredith.r.carter@gmail.com',
        'facebook': 'https://www.facebook.com/groups/1797296830528958/',
        'phone': 'tel:8177732826',
        'text': 'sms:8177732826',
        'vcard': 'https://app.elify.com/vcard/577r5l1p',
        'youtube': 'https://www.youtube.com/channel/UCwlDZwJAuigADGjo1IALhFQ'
    };
} else {
    social = {
        // 'facebook': 'https://www.facebook.com/Meredizzle87',
        'email': 'mailto:meredith.r.carter@gmail.com',
        'facebook': 'https://www.facebook.com/groups/1797296830528958/',
        'youtube': 'https://www.youtube.com/channel/UCwlDZwJAuigADGjo1IALhFQ'
    };
}

const Headshot = () =>
        <div>
            <Image src={headshotImage} size="medium" circular centered />
                <Header size="huge">
                    Meredith Carter
                    <Header.Subheader>
                        Thanks for stopping by!
                    </Header.Subheader>
                </Header>
            <a href={social.facebook} title="The Usborne Booktique Facebook Group" target="_blank"><Icon size="big" circular name="facebook" color="blue" inverted link /></a>
            <a href={social.youtube} title="Watch Me on YouTube!" target="_blank"><Icon size="big" circular name="youtube" color="red" inverted link /></a>
            {(isMobile.any()
                    ? <a href={social.phone} title="Call Me!" target="_blank"><Icon size="big" circular name="phone circle" color="purple" inverted link /></a>
                    : false
            )}
            {(isMobile.any()
                    ? <a href={social.text} title="Send Me A Text!" target="_blank"><Icon size="big" circular name="conversation circle" color="green" inverted link /></a>
                    : false
            )}
            <a href={social.vcard} title="Download My Contact Card" target="_blank"><Icon size="big" circular name="vcard" color="violet" inverted link /></a>
            <a href={social.email} title="Email Me!" target="_blank"><Icon size="big" circular name="mail" color="yellow" inverted link /></a>
        </div>;

export default Headshot;
