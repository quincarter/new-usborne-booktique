import React from 'react';
import { Card } from 'semantic-ui-react';
import fillerImage from '../assets/images/fillerImage.png';
import shopMyStore from '../assets/images/shopMyStore.jpg';
import joinMyTeam from '../assets/images/joinMyTeam.jpg';
import findEvent from '../assets/images/findEvent.jpg';

const images = {
    'filler': fillerImage,
    'find': findEvent,
    'join': joinMyTeam,
    'shop': shopMyStore
};

const ImageCards = () =>
    <div>
        <Card.Group stackable itemsPerRow={3}>
            <Card
                href="https://d5531.myubam.com/shop"
                header="Shop My Bookstore!"
                image={images.shop}
                description="Shop from thousands of books from the comfort of your computer!"
                raised
            />
            <Card
                href="https://d5531.myubam.com/EventPicker"
                header="Find your Event!"
                image={images.find}
                description="Hosting or attending an event? Click here!"
                raised
            />
            <Card
                href="https://d5531.myubam.com/Join"
                header="Want to join my team?"
                image={images.join}
                description="Why just host when you can sell awesome books?! Click here to join my team and get in touch with me!"
                raised
            />
        </Card.Group>
    </div>;

export default ImageCards;
